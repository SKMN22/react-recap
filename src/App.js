import React, { Component } from "react";
import Counters from "./components/Counters.jsx";
import FetchData from "./components/FetchData.jsx";
import NavBar from "./components/NavBar.jsx";

class App extends Component {
  state = {
    counters: [
      { id: 1, value: 0 },
      { id: 2, value: 1 },
      { id: 3, value: 2 },
      { id: 4, value: 3 },
      { id: 5, value: 4 },
    ],
  };

  constructor() {
    super();
    console.log("App - Constructor");
  }

  componentDidMount() {
    //Ajax Call
    console.log("App - Mounted");
  }

  handleDelete = (counterId) => {
    console.log("Handle delete", counterId);
    const newCounters = this.state.counters.filter(
      (counter) => counter !== counterId
    );
    this.setState({ newCounters });
  };

  handleReset = () => {
    this.state.counters.map((c) => {
      c.value = 0;
      return c;
    });
    this.setState(this.state.counters);
  };

  handleIncrement = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters });
  };
  render() {
    return (
      <React.Fragment>
        {/* <NavBar
          totalConuters={this.state.counters.filter((c) => c.value > 0).length}
        />
        <main className="container">
          <Counters
            counters={this.state.counters}
            onReset={this.handleReset}
            onIncrement={this.handleIncrement}
            onDelete={this.handleDelete}
          />
        </main> */}
        <FetchData />
      </React.Fragment>
    );
  }
}

export default App;
