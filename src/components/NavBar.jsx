import React from "react";

// stateless functional Component cannot use Lifecycle hooks
const NavBar = ({ totatlCounters }) => {
  return (
    <nav className="navbar navbar-light bg-light">
      <a className="navbar-brand" href="#">
        Navbar
        <span className="badge bage-pill badge-secondary">
          {totatlCounters}
        </span>
      </a>
    </nav>
  );
};

export default NavBar;
