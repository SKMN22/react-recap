import React, { Component } from "react";
import Counter from "./Counter";

class Counters extends Component {
  render() {
    const { handleReset, handleDelete, handleIncrement } = this.props;
    return (
      <div>
        <button onClick={handleReset} className="btn btn-primary btn-sm m-2">
          Reset
        </button>
        {this.props.counters.map((counter) => (
          <Counter
            key={counter.id}
            onDelete={handleDelete}
            onIncrement={handleIncrement}
            counter={counter}
          />
        ))}
      </div>
    );
  }
}

export default Counters;
