import React, { Component } from "react";

class FetchData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      isLoaded: false,
    };
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/todos/")
      .then((result) => result.json())
      .then((json) => {
        this.setState({
          isLoaded: true,
          items: json,
        });
      });
  }

  render() {
    const { isLoaded, items } = this.state;
    if (!isLoaded) {
      return <div>Loading</div>;
    } else {
      return (
        <div>
          <ul>
            {items.map((item) => {
              return (
                <li key={item.id}>
                  Title: {item.title} | Completed: {item.completed}
                </li>
              );
            })}
          </ul>
        </div>
      );
    }
  }
}

export default FetchData;
